#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "file.h"
#include "bmp.h"

void printMessage(const char* message, ...);
void throwError(const char* message, ...);
char* openStatusToMessage(const enum OpenStatus openStatus);
char* readStatusToMessage(const enum ReadStatus readStatus);
char* writeStatusToMessage(const enum WriteStatus writeStatus);
char* closeStatusToMessage(const enum CloseStatus closeStatus);

#endif 
