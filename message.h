#include "message.h"

void printMessage(const char* message, ...) {
    va_list args;
    va_start(args, message);
    vfprintf(stdout, message, args);
    fprintf(stdout, "\n");
    va_end(args);
}

void throwError(const char* message, ...) {
    va_list args;
    va_start(args, message);
    vfprintf(stderr, message, args);
    fprintf(stderr, "\n");
    va_end(args);
    exit(1);
}

char* openStatusToMessage(const enum OpenStatus openStatus) {
    switch (openStatus) {
    case OPEN_CHECK: return "File was opened";
    case OPEN_ERROR: return "File opening error";
    default: return "Unknown file opening error.";
    }
}

char* readStatusToMessage(const enum ReadStatus readStatus) {
    switch (readStatus) {
    case READ_CHECK: return "File was read";
    case READ_INVALID_BITS: return "Invalid bits error.";
    case READ_INVALID_HEADER: return "Invalid header error.";
    case READ_INVALID_SIGNATURE: return "Invalid signature error.";
    case READ_UNSUPPORTED_DEPTH: return "Unsupported color depth error - only 24 bits supported.";
    default: return "Unknown file reading error.";
    }
}

char* writeStatusToMessage(const enum WriteStatus writeStatus) {
    switch (writeStatus) {
    case WRITE_CHECK: return "File was written successfully.";
    case WRITE_ERROR: return "File writing error.";
    case WRITE_INVALID_BITS: return "Invalid bits error.";
    default: return "Unknown file writing error.";
    }
}

char* closeStatusToMessage(const enum CloseStatus closeStatus) {
    switch (closeStatus) {
    case CLOSE_CHECK: return "File was closed";
    case CLOSE_ERROR: return "Closing error";
    default: return "Unknown closing error.";
    }
}
