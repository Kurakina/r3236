#ifndef ROTATELEFT_90_
#define ROTATELEFT_90_

#include <stdlib.h>
#include "picture.h"

struct Picture rotateLeft90(struct Picture const source);

#endif