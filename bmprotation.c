#define _CRT_SECURE_NO_WARNINGS
#include "file.h"

enum OpenStatus openFile(FILE** file, const char* path) {
    *file = fopen(path, "r+b");
    if (*file == NULL) return OPEN_ERROR;
    return OPEN_CHECK;
}

enum CloseStatus closeFile(FILE** file) {
    if (fclose(*file) != 0) return CLOSE_ERROR;
    return CLOSE_CHECK;
}
