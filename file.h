﻿#include "bmp.h"
#include "rotateleft90.h"
#include "file.h"
#include "message.h"

int main(int argc, char** argv) 
{
    if (argc != 2 || argv[1] == NULL) 
        throwError("Data error!");
    if (argc > 2) 
        throwError("Too many arguments!");
    if (argc < 2) 
        throwError("Not enough arguments!");
    struct Picture _picture;
    FILE* pictureFile = NULL;
    enum OpenStatus openStatus = openFile(&pictureFile, argv[1]);
    if (openStatus != OPEN_CHECK)
        throwError("There is an error with opennig the file", openStatus, openStatusToMessage(openStatus));
    enum ReadStatus readStatus = fromBmp(pictureFile, &_picture);
    if (readStatus != READ_CHECK)
        throwError("There is an error with reading the file", readStatus, readStatusToMessage(readStatus));
    struct Picture newPicture = rotateLeft90(_picture);
    enum WriteStatus writeStatus = toBmp(pictureFile, &newPicture);
    if (writeStatus != WRITE_CHECK)
        throwError("There is an error with writing the file", writeStatus, writeStatusToMessage(writeStatus));
    enum CloseStatus closeStatus = closeFile(&pictureFile);
    if (closeStatus != CLOSE_CHECK)
        throwError("There is an error with closing the file", closeStatus, closeStatusToMessage(closeStatus));

    printf("Success!");
    return 0;
}
