#include "bmp.h"

struct BmpHeader createBmpHeader(struct Picture const pic) {
    struct BmpHeader header;

    uint32_t headerSize = sizeof(struct BmpHeader);
    uint32_t pixelSize = sizeof(struct Pixel);

    header.bfType = BMP_TYPE;
    header.biBitCount = COLOR_DEPTH;
    header.biHeight = pic.h;
    header.biWidth = pic.w;
    header.bOffBits = headerSize;
    header.bfileSize = headerSize + (pixelSize * pic.w + pic.w % 4) * pic.h;
    header.biSizeImage = pic.w * pic.h * pixelSize;
    header.biSize = 40;
    header.biPlanes = 1;
    header.bfReserved = 0;
    header.biCompression = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

uint32_t getPadding(const uint64_t picWidth) {
    size_t widthSize = picWidth * sizeof(struct Pixel);
    if (widthSize % PADDING_SIZE == 0) return 0;
    return PADDING_SIZE - (widthSize % PADDING_SIZE);
}

enum ReadStatus fromBmp(FILE* in, struct Picture* pic) {
    struct BmpHeader header;

    uint32_t headerSize = sizeof(struct BmpHeader);
    uint32_t pixelSize = sizeof(struct Pixel);

    if (fread(&header, headerSize, 1, in) < 1) {
        if (feof(in)) return READ_INVALID_BITS;
        return READ_INVALID_HEADER;
    }
    if (fseek(in, header.bOffBits, SEEK_SET) != 0) return READ_INVALID_BITS;
    if (header.biBitCount != COLOR_DEPTH) return READ_UNSUPPORTED_DEPTH;
    if (header.bfType != BMP_TYPE) return READ_INVALID_SIGNATURE;

    pic->h = header.biHeight;
    pic->w = header.biWidth;
    pic->data = malloc(pic->w * pic->h * pixelSize);

    uint32_t padding = getPadding(pic->w);

    for (size_t y = 0; y < pic->h; ++y) {
        if (fread(pic->data + y * pic->w, pixelSize, pic->w, in) < pixelSize) return READ_INVALID_BITS;
        if (fseek(in, padding, SEEK_CUR) != 0) return READ_INVALID_BITS;
    }

    if (fseek(in, 0, SEEK_SET) != 0) return READ_INVALID_BITS;
    return READ_CHECK;
}

enum WriteStatus toBmp(FILE* out, struct Picture const* pic) {
    struct BmpHeader header = createBmpHeader(*pic);

    uint32_t headerSize = sizeof(struct BmpHeader);
    uint32_t pixelSize = sizeof(struct Pixel);

    if (fwrite(&header, headerSize, 1, out) < 1) return WRITE_ERROR;

    char paddingBytes[3] = { 0 };
    uint32_t padding = getPadding(pic->w);

    for (size_t y = 0; y < pic->h; ++y) {
        if (fwrite(pic->data + y * pic->w, pixelSize, pic->w, out) != pic->w) return WRITE_ERROR;
        if (fflush(out) != 0) return WRITE_ERROR;
        if (fwrite(paddingBytes, padding, 1, out) != 1 && padding != 0) return WRITE_ERROR;
        if (fflush(out) != 0) return WRITE_ERROR;
    }

    if (fseek(out, 0, SEEK_SET) != 0) return WRITE_ERROR;
    return WRITE_CHECK;
}
