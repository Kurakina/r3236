#ifndef FILE_H_
#define FILE_H_

#include <stdio.h>

enum OpenStatus {
    OPEN_CHECK = 0,
    OPEN_ERROR
};

enum OpenStatus openFile(FILE** file, const char* path);

enum CloseStatus {
    CLOSE_CHECK = 0,
    CLOSE_ERROR
};

enum CloseStatus closeFile(FILE** file);

#endif
